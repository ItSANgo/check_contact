-- get list.
SELECT
    lines.ticket_id
    , lines.line_id
    , tickets.name AS ticket_name
    , tickets.detail AS ticket_detail
    , tickets.opened_time AS ticket_opened_time
    , tickets.closed_time AS ticket_closed_time
    , tickets.added_time AS ticket_added_time
    , tickets.added_user AS ticket_added_user
    , tickets.modified_time AS ticket_modified_time
    , tickets.modified_user AS ticket_modified_user
    , lines.name AS line_name
    , lines.detail AS line_detail
    , lines.usual_number AS line_usual_number
    , lines.night_number AS line_night_number
    , lines.override_number AS line_override_number
    , lines.opened_time AS line_opened_time
    , lines.closed_time AS line_closed_time
    , lines.added_time AS line_added_time
    , lines.added_user AS line_added_user
    , lines.modified_time AS line_modified_time
    , lines.modified_user AS line_modified_user
    , status_null
    , status_open
    , status_start
    , status_progress
    , status_pending
    , status_end
    , status_close
FROM
    lines
    , tickets
    , (
        SELECT
            lines_histories.ticket_id
            , lines_histories.line_id
            , MAX(CASE WHEN lines_histories.status IS NULL THEN 0 ELSE NULL END) AS status_null
            , MAX(CASE lines_histories.status WHEN 5 THEN lines_histories.history_id ELSE NULL END) AS status_open
            , MAX(CASE lines_histories.status WHEN 1 THEN lines_histories.history_id ELSE NULL END) AS status_start
            , MAX(CASE lines_histories.status WHEN 3 THEN lines_histories.history_id ELSE NULL END) AS status_progress
            , MAX(CASE lines_histories.status WHEN 6 THEN lines_histories.history_id ELSE NULL END) AS status_pending
            , MAX(CASE lines_histories.status WHEN 2 THEN lines_histories.history_id ELSE NULL END) AS status_end
            , MAX(CASE lines_histories.status WHEN 7 THEN lines_histories.history_id ELSE NULL END) AS status_close
        FROM
            (
                SELECT
                    lines.ticket_id
                    , lines.line_id
                    , histories.history_id
                    , histories.status
                FROM
                    lines LEFT JOIN histories
                    ON (lines.ticket_id = histories.ticket_id
                    AND lines.line_id = histories.line_id)
            ) lines_histories
        GROUP BY
            lines_histories.ticket_id
            , lines_histories.line_id
    ) summaries
WHERE lines.ticket_id = tickets.id
AND lines.ticket_id = summaries.ticket_id
AND lines.line_id = summaries.line_id
;
