#! /usr/bin/make -f

DESTDIR=$(HOME)/public_html
CGI_DIR=$(DESTDIR)/cgi-bin
JAVASCRIPT_DIR=$(DESTDIR)/javascript
CSS_DIR=$(DESTDIR)/css

PERL_LIB_DIR=$(CGI_DIR)

APACHE_ACL_DIR=/etc/apache2/sites-enabled

TARGETS=init/build/CheckContactCredentials.pl
INSTALL_TARGETS=$(CGI_DIR)/list.cgi
INSTALL_DEPENDS=$(PERL_LIB_DIR)/CheckContactCredentials.pl \
	$(JAVASCRIPT_DIR)/list.js $(CSS_DIR)/css.css
INSTALL_APACHE_ACL=$(APACHE_ACL_DIR)/010-user_cgi.conf

.PHONY: all builddb install-user install uninstall uninstall-user clean
all: $(TARGETS)
init/build/CheckContactCredentials.pl: ./init/src/main/bash/0030_create_perl_credential.bash ./init/src/main/credentials/dbpass.bash
	./init/src/main/bash/0030_create_perl_credential.bash
install-user: $(INSTALL_TARGETS) builddb
$(CGI_DIR)/list.cgi: cgi/src/main/cgi-bin/list.cgi $(INSTALL_DEPENDS)
	install -m 755 cgi/src/main/cgi-bin/list.cgi $(CGI_DIR)
$(JAVASCRIPT_DIR)/list.js: cgi/src/main/javascript/list.js
	install -m 644 cgi/src/main/javascript/list.js $(JAVASCRIPT_DIR)
$(CSS_DIR)/css.css: cgi/src/main/css/css.css
	install -m 644 cgi/src/main/css/css.css $(CSS_DIR)
$(PERL_LIB_DIR)/CheckContactCredentials.pl: init/build/CheckContactCredentials.pl
	install -m 644 init/build/CheckContactCredentials.pl $(PERL_LIB_DIR)
builddb: ./init/src/main/bash/0010_create_db.bash ./init/src/main/bash/0020_create_tables.bash ./init/src/main/bash/0040_insert_data.bash
	 ./init/src/main/bash/0010_create_db.bash
	 ./init/src/main/bash/0020_create_tables.bash
	 ./init/src/main/bash/0040_insert_data.bash
install: $(INSTALL_APACHE_ACL)
$(INSTALL_APACHE_ACL): apache-conf/src/main/conf/010-user_cgi.conf
	install -m 644 apache-conf/src/main/conf/010-user_cgi.conf $(APACHE_ACL_DIR)
uninstall-user: clean
	rm -f $(INSTALL_TARGETS) $(INSTALL_DEPENDS)
clean:
	rm -f $(TARGETS)
uninstall:
	rm -f $(INSTALL_APACHE_ACL)
