#! /bin/bash -eu

directory=$(dirname "$0")
credentials_dir="$directory/../../credentials"
source "$credentials_dir/dbpass.bash"

connection="postgresql://$dbadmin:$dbadmin_password@/$database"

psql "$connection"  <<END_OF_DDL
CREATE TABLE codes (
    code_type INTEGER
    , id INTEGER
    , order_number INTEGER
    , name VARCHAR(120) NOT NULL
    , added_time TIMESTAMP NOT NULL
    , added_user VARCHAR(64) NOT NULL
    , modified_time TIMESTAMP NOT NULL
    , modified_user VARCHAR(64) NOT NULL
    , PRIMARY KEY(code_type, id)
);
CREATE TABLE tickets (
    id INTEGER
    , name VARCHAR(1000) NOT NULL
    , detail VARCHAR(1000)
    , opened_time TIMESTAMP NOT NULL
    , closed_time TIMESTAMP
    , added_time TIMESTAMP NOT NULL
    , added_user VARCHAR(64) NOT NULL
    , modified_time TIMESTAMP NOT NULL
    , modified_user VARCHAR(64) NOT NULL
    , PRIMARY KEY(id)
);
CREATE TABLE lines (
    ticket_id INTEGER
    , line_id INTEGER
    , name VARCHAR(1000) NOT NULL
    , detail VARCHAR(1000)
    , usual_number VARCHAR(16)
    , night_number VARCHAR(16)
    , override_number VARCHAR(16)
    , opened_time TIMESTAMP NOT NULL
    , closed_time TIMESTAMP
    , added_time TIMESTAMP NOT NULL
    , added_user VARCHAR(64) NOT NULL
    , modified_time TIMESTAMP NOT NULL
    , modified_user VARCHAR(64) NOT NULL
    , PRIMARY KEY(ticket_id, line_id)
);
CREATE TABLE histories (
    ticket_id INTEGER
    , line_id INTEGER
    , history_id INTEGER
    , status INTEGER
    , detail VARCHAR(1000)
    , added_time TIMESTAMP NOT NULL
    , added_user VARCHAR(64) NOT NULL
    , modified_time TIMESTAMP NOT NULL
    , modified_user VARCHAR(64) NOT NULL
    , PRIMARY KEY(ticket_id, line_id, history_id)
);
END_OF_DDL
