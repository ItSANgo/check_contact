#! /bin/bash -eu

directory=$(dirname "$0")
credentials_dir="$directory/../../credentials"
source "$credentials_dir/dbpass.bash"

sudo -u postgres psql <<END_OF_CREATE_ROLE
DROP DATABASE $database;
DROP ROLE $dbadmin;
CREATE ROLE $dbadmin LOGIN CREATEDB CREATEROLE;
ALTER USER $dbadmin WITH PASSWORD '$dbadmin_password';
CREATE DATABASE $database OWNER $dbadmin;
END_OF_CREATE_ROLE
