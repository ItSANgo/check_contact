#! /bin/bash -eu

init_root=$(dirname "$0")
cd "$init_root/0020_create_tables.d"

for i in [0-9]*.bash
do
    "./$i"
done
