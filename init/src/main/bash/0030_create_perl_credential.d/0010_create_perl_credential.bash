#! /bin/bash -eu

directory=$(dirname "$0")
build_dir=${build_dir-"$directory/../../../../build"}

credentials_dir="$directory/../../credentials"
source "$credentials_dir/dbpass.bash"

install -m 755 -d "$build_dir"

cat <<END_OF_PERL_CREDENTIALS >"$build_dir/CheckContactCredentials.pl"
package CheckContactCredentials;
our @EXPORT = qw(data_source db_user db_password);
sub CheckContactCredentials::data_source() { return 'dbi:Pg:dbname=$database;host=localhost;port=5432'; }
sub CheckContactCredentials::db_user() { return '$dbadmin'; }
sub CheckContactCredentials::db_password() { return '$dbadmin_password'; }
1;
END_OF_PERL_CREDENTIALS
