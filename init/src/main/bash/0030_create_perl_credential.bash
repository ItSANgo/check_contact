#! /bin/bash -eu

init_root=$(dirname "$0")
cd "$init_root/0030_create_perl_credential.d"

for i in [0-9]*.bash
do
    "./$i"
done
