#! /bin/bash -eu

directory=$(dirname "$0")
credentials_dir="$directory/../../credentials"
source "$credentials_dir/dbpass.bash"

connection="postgresql://$dbadmin:$dbadmin_password@/$database"

psql "$connection" <<END_OF_TEST_DATA
INSERT INTO codes (code_type, id, order_number, name
, added_time, added_user, modified_time, modified_user)
VALUES
    (1, 1, 2, '*start*'
    , '2022-01-01 00:00', '$dbadmin', '2022-01-01 00:00', '$dbadmin')
    , (1, 2, 5, '*end*'
    , '2022-01-01 00:00', '$dbadmin', '2022-01-01 00:00', '$dbadmin')
    , (1, 3, 3, '*progres*'
    , '2022-01-01 00:00', '$dbadmin', '2022-01-01 00:00', '$dbadmin')
    , (1, 4, 7, 'comment'
    , '2022-01-01 00:00', '$dbadmin', '2022-01-01 00:00', '$dbadmin')
    , (1, 5, 1, '*open*'
    , '2022-01-01 00:00', '$dbadmin', '2022-01-01 00:00', '$dbadmin')
    , (1, 6, 4, '*pending*'
    , '2022-01-01 00:00', '$dbadmin', '2022-01-01 00:00', '$dbadmin')
    , (1, 7, 6, '*close*'
    , '2022-01-01 00:00', '$dbadmin', '2022-01-01 00:00', '$dbadmin')
    , (2, 1, 1, 'others'
    , '2022-01-01 00:00', '$dbadmin', '2022-01-01 00:00', '$dbadmin')
;
INSERT INTO tickets (id, name, detail, opened_time, closed_time
, added_time, added_user, modified_time, modified_user)
VALUES
    (1, 'trouble 1', NULL, '2022-01-02 10:01', NULL
    , '2022-01-02 10:01', '$dbadmin', '2022-01-02 10:01', '$dbadmin')
    , (2, 'trouble 2', '2 troubles', '2022-01-03 11:02', NULL
    , '2022-01-03 11:02', '$dbadmin', '2022-01-03 11:03', '$dbadmin')
    , (3, '3 report', 'a report', '2022-01-04 12:04', NULL
    , '2022-01-04 12:03', '$dbadmin', '2022-01-04 12:03', '$dbadmin')
    , (4, '4 closed', 'closed', '2022-01-05 13:04', '2022-01-06 14:05'
    , '2022-01-05 13:04', '$dbadmin', '2022-01-06 14:05', '$dbadmin')
    , (5, '5 trouble 5', 'troubling', '2022-01-08 13:04', NULL
    , '2022-01-08 13:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
;
INSERT INTO lines (ticket_id, line_id, name, detail
, usual_number, night_number, override_number, opened_time, closed_time
, added_time, added_user, modified_time, modified_user)
VALUES
    (1, 1, 'line 1', 'trouble 1'
    , '000-0000-0000', '000-0000-0001', '000-0000-0002'
    , '2022-01-02 10:01', NULL
    , '2022-01-02 10:01', '$dbadmin', '2022-01-02 10:01', '$dbadmin')
    , (2, 1, 'line 2-1', NULL
    , '000-0001-0003', '000-0001-0004', '000-0001-0005'
    , '2022-01-03 11:02', NULL
    , '2022-01-03 11:02', '$dbadmin', '2022-01-03 11:02', '$dbadmin')
    , (2, 2, 'line 2-2', NULL
    , '000-0002-0006', '000-0001-0007', '000-0001-0008'
    , '2022-01-03 11:03', NULL
    , '2022-01-03 11:03', '$dbadmin', '2022-01-03 11:04', '$dbadmin')
    , (4, 1, 'line 4-1', 'closed 4-1'
    , '000-0004-0009', '000-0004-0010', '000-0004-0011'
    , '2022-01-05 13:04', NULL
    , '2022-01-05 13:04', '$dbadmin', '2022-01-06 14:05', '$dbadmin')
    , (4, 2, 'line 4-2', NULL
    , '000-0042-0001', '000-0042-0002', '000-0042-0003'
    , '2022-01-05 13:04:01', '2022-01-05 13:04:02'
    , '2022-01-05 13:04:01', '$dbadmin', '2022-01-05 13:04:02', '$dbadmin')
    , (5, 1, 'line 5-1', NULL
    , '000-0005-0009', '000-0005-0010', '000-0005-0011'
    , '2022-01-08 13:04', NULL
    , '2022-01-08 13:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 2, 'line 5-2', NULL
    , '000-0052-0001', '000-0052-0002', '000-0052-0003'
    , '2022-01-08 13:04:01', '2022-01-08 13:04:02'
    , '2022-01-08 13:04:01', '$dbadmin', '2022-01-08 13:04:02', '$dbadmin')
    , (5, 3, 'line 5-3', NULL
    , '000-0005-0009', '000-0005-0010', '000-0005-0011'
    , '2022-01-08 13:04', NULL
    , '2022-01-08 13:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 4, 'line 5-4', NULL
    , '000-0054-0001', '000-0054-0002', '000-0054-0003'
    , '2022-01-08 13:04:01', '2022-01-08 13:04:02'
    , '2022-01-08 13:04:01', '$dbadmin', '2022-01-08 13:04:02', '$dbadmin')
    , (5, 5, 'line 5-5', NULL
    , '000-0005-0009', '000-0005-0010', '000-0005-0011'
    , '2022-01-08 13:04', NULL
    , '2022-01-08 13:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 6, 'line 5-6', NULL
    , '000-0056-0001', '000-0056-0002', '000-0056-0003'
    , '2022-01-08 13:04:01', '2022-01-08 13:04:02'
    , '2022-01-08 13:04:01', '$dbadmin', '2022-01-08 13:04:02', '$dbadmin')
    , (5, 7, 'line 5-7', NULL
    , '000-0005-0009', '000-0005-0010', '000-0005-0011'
    , '2022-01-08 13:04', NULL
    , '2022-01-08 13:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 8, 'line 5-8', NULL
    , '000-0058-0001', '000-0058-0002', '000-0058-0003'
    , '2022-01-08 13:04:01', '2022-01-08 13:04:02'
    , '2022-01-08 13:04:01', '$dbadmin', '2022-01-08 13:04:02', '$dbadmin')
    , (5, 9, 'line 5-9', NULL
    , '000-0005-0009', '000-0005-0010', '000-0005-0011'
    , '2022-01-08 13:04', NULL
    , '2022-01-08 13:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 10, 'line 5-10', NULL
    , '000-0005-0009', '000-0005-0010', '000-0005-0011'
    , '2022-01-08 13:04', NULL
    , '2022-01-08 13:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 11, 'line 5-11', NULL
    , '000-0511-0001', '000-0511-0002', '000-0511-0003'
    , '2022-01-08 13:04:01', '2022-01-08 13:04:02'
    , '2022-01-08 13:04:01', '$dbadmin', '2022-01-08 13:04:02', '$dbadmin')
    , (5, 12, 'line 5-12', NULL
    , '000-0512-0009', '000-0512-0010', '000-0512-0011'
    , '2022-01-08 13:04', NULL
    , '2022-01-08 13:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 13, 'line 5-13', NULL
    , '000-0513-0001', '000-0513-0002', '000-0513-0003'
    , '2022-01-08 13:04:01', '2022-01-08 13:04:02'
    , '2022-01-08 13:04:01', '$dbadmin', '2022-01-08 13:04:02', '$dbadmin')
    , (5, 14, 'line 5-14', NULL
    , '000-0514-0009', '000-0514-0010', '000-0514-0011'
    , '2022-01-08 13:04', NULL
    , '2022-01-08 13:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 15, 'line 5-15', NULL
    , '000-0515-0001', '000-0515-0002', '000-0515-0003'
    , '2022-01-08 13:04:01', '2022-01-08 13:04:02'
    , '2022-01-08 13:04:01', '$dbadmin', '2022-01-08 13:04:02', '$dbadmin')
    , (5, 16, 'line 5-16', NULL
    , '000-0516-0009', '000-0516-0010', '000-0516-0011'
    , '2022-01-08 13:04', NULL
    , '2022-01-08 13:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 17, 'line 5-17', NULL
    , '000-0517-0001', '000-0517-0002', '000-0517-0003'
    , '2022-01-08 13:04:01', '2022-01-08 13:04:02'
    , '2022-01-08 13:04:01', '$dbadmin', '2022-01-08 13:04:02', '$dbadmin')
    , (5, 18, 'line 5-18', NULL
    , '000-0518-0009', '000-0518-0010', '000-0518-0011'
    , '2022-01-08 13:04', NULL
    , '2022-01-08 13:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 19, 'line 5-19', NULL
    , '000-0519-0001', '000-0519-0002', '000-0519-0003'
    , '2022-01-08 13:04:01', '2022-01-08 13:04:02'
    , '2022-01-08 13:04:01', '$dbadmin', '2022-01-08 13:04:02', '$dbadmin')
    , (5, 20, 'line 5-20', NULL
    , '000-0520-0001', '000-0520-0002', '000-0520-0003'
    , '2022-01-08 13:04:01', '2022-01-08 13:04:02'
    , '2022-01-08 13:04:01', '$dbadmin', '2022-01-08 13:04:02', '$dbadmin')
    , (5, 21, 'line 5-21', NULL
    , '000-0521-0001', '000-0521-0002', '000-0521-0003'
    , '2022-01-08 23:04:01', '2022-01-08 23:04:02'
    , '2022-01-08 23:04:01', '$dbadmin', '2022-01-08 23:04:02', '$dbadmin')
    , (5, 22, 'line 5-22', NULL
    , '000-0522-0009', '000-0522-0010', '000-0522-0021'
    , '2022-01-08 23:04', NULL
    , '2022-01-08 23:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 23, 'line 5-23', NULL
    , '000-0523-0001', '000-0523-0002', '000-0523-0003'
    , '2022-01-08 23:04:01', '2022-01-08 23:04:02'
    , '2022-01-08 23:04:01', '$dbadmin', '2022-01-08 23:04:02', '$dbadmin')
    , (5, 24, 'line 5-24', NULL
    , '000-0524-0009', '000-0524-0010', '000-0524-0021'
    , '2022-01-08 23:04', NULL
    , '2022-01-08 23:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 25, 'line 5-25', NULL
    , '000-0525-0001', '000-0525-0002', '000-0525-0003'
    , '2022-01-08 23:04:01', '2022-01-08 23:04:02'
    , '2022-01-08 23:04:01', '$dbadmin', '2022-01-08 23:04:02', '$dbadmin')
    , (5, 26, 'line 5-26', NULL
    , '000-0526-0009', '000-0526-0010', '000-0526-0021'
    , '2022-01-08 23:04', NULL
    , '2022-01-08 23:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 27, 'line 5-27', NULL
    , '000-0527-0001', '000-0527-0002', '000-0527-0003'
    , '2022-01-08 23:04:01', '2022-01-08 23:04:02'
    , '2022-01-08 23:04:01', '$dbadmin', '2022-01-08 23:04:02', '$dbadmin')
    , (5, 28, 'line 5-28', NULL
    , '000-0528-0009', '000-0528-0010', '000-0528-0021'
    , '2022-01-08 23:04', NULL
    , '2022-01-08 23:04', '$dbadmin', '2022-01-08 14:05', '$dbadmin')
    , (5, 29, 'line 5-29', NULL
    , '000-0529-0001', '000-0529-0002', '000-0529-0003'
    , '2022-01-08 23:04:01', '2022-01-08 23:04:02'
    , '2022-01-08 23:04:01', '$dbadmin', '2022-01-08 23:04:02', '$dbadmin')
    , (5, 30, 'line 5-30', NULL
    , '000-0530-0001', '000-0530-0002', '000-0530-0003'
    , '2022-01-08 13:04:01', '2022-01-08 13:04:02'
    , '2022-01-08 13:04:01', '$dbadmin', '2022-01-08 13:04:02', '$dbadmin')
;
INSERT INTO histories (ticket_id, line_id, history_id, status, detail
, added_time, added_user, modified_time, modified_user)
VALUES
    (1, 1, 1, 5, 'line 1 opened'
    , '2022-01-02 10:01', '$dbadmin', '2022-01-02 10:01', '$dbadmin')
    , (2, 1, 1, 5, 'trouble 2 line 1 opened'
    , '2022-01-03 11:02', '$dbadmin', '2022-01-03 11:02', '$dbadmin')
    , (2, 2, 1, 5, 'trouble 2 line 2 opened'
    , '2022-01-03 11:03', '$dbadmin', '2022-01-03 11:03', '$dbadmin')
    , (2, 2, 2, 1, 'trouble 2 line 2 started'
    , '2022-01-03 11:04', '$dbadmin', '2022-01-03 11:04', '$dbadmin')
    , (4, 1, 1, 5, 'closed line 1 opened'
    , '2022-01-05 13:04', '$dbadmin', '2022-01-05 13:04', '$dbadmin')
    , (4, 1, 2, 1, 'closed line 1 started'
    , '2022-01-05 13:04', '$dbadmin', '2022-01-05 13:04', '$dbadmin')
    , (4, 1, 3, 3, 'closed line 1 progres'
    , '2022-01-05 13:04', '$dbadmin', '2022-01-05 13:04', '$dbadmin')
    , (4, 1, 4, 6, 'closed line 1 pending'
    , '2022-01-05 13:04', '$dbadmin', '2022-01-05 13:04', '$dbadmin')
    , (4, 1, 5, 4, 'closed line 1 pending'
    , '2022-01-05 13:04', '$dbadmin', '2022-01-05 13:04', '$dbadmin')
    , (4, 1, 6, 2, 'closed line 1 ended'
    , '2022-01-05 13:04', '$dbadmin', '2022-01-05 13:04', '$dbadmin')
    , (4, 1, 7, 7, 'closed line 1 closed'
    , '2022-01-05 13:04', '$dbadmin', '2022-01-05 13:04', '$dbadmin')
;
END_OF_TEST_DATA
