///

let selectedCell
const cells = document.querySelector('#list-body').querySelectorAll('td')
cells.forEach(function (cell) {
    cell.classList.add('nowrap')
    cell.classList.add('unselected-td')
    cell.onclick = function (ev) {
        if (selectedCell != null) {
            const previousRowClasses = selectedCell.parentElement.classList
            previousRowClasses.remove('selected-tr')
            previousRowClasses.add('unselected-tr')
            const previousCellClassList = selectedCell.classList
            previousCellClassList.remove('selected-td')
            previousCellClassList.add('unselected-td')
        }
        const target = ev.target
        const targetClasses = target.classList
        targetClasses.remove('unselected-td')
        targetClasses.add('selected-td')
        const targetRowClasses = target.parentElement.classList
        targetRowClasses.remove('unselected-tr')
        targetRowClasses.add('selected-tr')
        selectedCell = target
    }
})

document.querySelector('#detail').onclick = function (ev) {
    if (selectedCell == null) { return }
    const row = selectedCell.parentElement
    const ids = selectedCell.id.split('-')
    const ticket_id = ids[1].substring(1)
    const line_id = ids[2].substring(2)

    if (ids.length > 4) {
        const value = ids[4]
        if (value != 'undef') {
            const hisotry_id = value

        } else {

        }
    } else {

    }
    // FIXME: これから.
}