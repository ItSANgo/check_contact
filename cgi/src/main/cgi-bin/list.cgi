#! /usr/bin/perl

use strict;
use warnings;
use DBI;
push(@INC, '.');
require 'CheckContactCredentials.pl';
my $data_source = CheckContactCredentials::data_source();
my $db_user = CheckContactCredentials::db_user();
my $db_password = CheckContactCredentials::db_password();
# my (@driver_names) = DBI->available_drivers;
# print STDOUT "names: @driver_names";

my $query = <<END_OF_QUERY;
SELECT
    lines.ticket_id
    , lines.line_id
    , tickets.name AS ticket_name
    , tickets.detail AS ticket_detail
    , tickets.opened_time AS ticket_opened_time
    , tickets.closed_time AS ticket_closed_time
    , tickets.added_time AS ticket_added_time
    , tickets.added_user AS ticket_added_user
    , tickets.modified_time AS ticket_modified_time
    , tickets.modified_user AS ticket_modified_user
    , lines.name AS line_name
    , lines.detail AS line_detail
    , lines.usual_number AS line_usual_number
    , lines.night_number AS line_night_number
    , lines.override_number AS line_override_number
    , lines.opened_time AS line_opened_time
    , lines.closed_time AS line_closed_time
    , lines.added_time AS line_added_time
    , lines.added_user AS line_added_user
    , lines.modified_time AS line_modified_time
    , lines.modified_user AS line_modified_user
    , status_open
    , status_start
    , status_progress
    , status_pending
    , status_others
    , status_end
    , status_close
    , status_null
FROM
    lines
    , tickets
    , (
        SELECT
            lines_histories.ticket_id
            , lines_histories.line_id
            , MAX(CASE lines_histories.status WHEN 5 THEN lines_histories.history_id ELSE NULL END) AS status_open
            , MAX(CASE lines_histories.status WHEN 1 THEN lines_histories.history_id ELSE NULL END) AS status_start
            , MAX(CASE lines_histories.status WHEN 3 THEN lines_histories.history_id ELSE NULL END) AS status_progress
            , MAX(CASE lines_histories.status WHEN 6 THEN lines_histories.history_id ELSE NULL END) AS status_pending
            , MAX(CASE WHEN lines_histories.status IN (5, 1, 3, 6, 2, 7) THEN NULL ELSE lines_histories.history_id END) AS status_others
            , MAX(CASE lines_histories.status WHEN 2 THEN lines_histories.history_id ELSE NULL END) AS status_end
            , MAX(CASE lines_histories.status WHEN 7 THEN lines_histories.history_id ELSE NULL END) AS status_close
            , MAX(CASE WHEN lines_histories.status IS NULL THEN 0 ELSE NULL END) AS status_null
        FROM
            (
                SELECT
                    lines.ticket_id
                    , lines.line_id
                    , histories.history_id
                    , histories.status
                FROM
                    lines LEFT JOIN histories
                    ON (lines.ticket_id = histories.ticket_id
                    AND lines.line_id = histories.line_id)
            ) lines_histories
        GROUP BY
            lines_histories.ticket_id
            , lines_histories.line_id
    ) summaries
WHERE lines.ticket_id = tickets.id
AND lines.ticket_id = summaries.ticket_id
AND lines.line_id = summaries.line_id
END_OF_QUERY


my $dbh = DBI->connect($data_source, $db_user, $db_password)
or die $DBI::errstr;
my $sth = $dbh->prepare($query) or die $dbh->errstr;
my $rv = $sth->execute or die $sth->errstr;

my @line_columns = (
    'ticket_id',
    'line_id',
    'ticket_name',
    'ticket_detail',
    'ticket_opened_time',
    'ticket_closed_time',
    'ticket_added_time',
    'ticket_added_user',
    'ticket_modified_time',
    'ticket_modified_user',
    'line_name',
    'line_detail',
    'line_usual_number',
    'line_night_number',
    'line_override_number',
    'line_opened_time',
    'line_closed_time',
    'line_added_time',
    'line_added_user',
    'line_modified_time',
    'line_modified_user'
);

my @status_columns = (
    'status_open',
    'status_start',
    'status_progress',
    'status_pending',
    'status_others',
    'status_end',
    'status_close',
    'status_null',
);

print STDOUT <<END_OF_HEAD;
Content-Type: text/html

<!DOCTYPE html>
<html lang="ja">
    <head>
        <title lang="en">list</title>
        <meta http-equiv="X-UA-Compatible" content="IE=11" />
        <link href="../css/css.css" rel="stylesheet" />
    </head>
    <body>
        <main>
            <form id="detail" action="detail.pl">
                <div id="data_panel">
                    <table id="list">
                        <thead lang="en">
                            <tr id="list-head">
                                <th id="ticket_id" class="ticket_id ticket">ticket_id</th>
                                <th id="line_id" class="line_id line">line_id</th>
                                <th id="ticket_name" class="ticket_name ticket">ticket_name</th>
                                <th id="ticket_detail" class="ticket_detail ticket">ticket_detail</th>
                                <th id="ticket_opened_time" class="ticket_opened_time ticket">ticket_opened_time</th>
                                <th id="ticket_closed_time" class="ticket_closed_time ticket">ticket_closed_time</th>
                                <th id="ticket_added_time" class="ticket_added_time ticket">ticket_added_time</th>
                                <th id="ticket_added_user" class="ticket_added_user ticket">ticket_added_user</th>
                                <th id="ticket_modified_time" class="ticket_modified_time ticket">ticket_modified_time</th>
                                <th id="ticket_modified_user" class="ticket_modified_user ticket">ticket_modified_user</th>
                                <th id="line_name" class="line_name line">line_name</th>
                                <th id="line_detail" class="line_detail line">line_detail</th>
                                <th id="line_usual_number" class="line_usual_number line">line_usual_number</th>
                                <th id="line_night_number" class="line_night_number line">line_night_number</th>
                                <th id="line_override_number" class="line_override_number line">line_override_number</th>
                                <th id="line_opened_time" class="line_opened_time line">line_opened_time</th>
                                <th id="line_closed_time" class="line_closed_time line">line_closed_time</th>
                                <th id="line_added_time" class="line_added_time line">line_added_time</th>
                                <th id="line_added_user" class="line_added_user line">line_added_user</th>
                                <th id="line_modified_time" class="line_modified_time line">line_modified_time</th>
                                <th id="line_modified_user" class="line_modified_user line">line_modified_user</th>
                                <th id="status_open" class="status_open history">status_open</th>
                                <th id="status_start" class="status_start history">status_start</th>
                                <th id="status_progress" class="status_progress history">status_progress</th>
                                <th id="status_pending" class="status_pending history">status_pending</th>
                                <th id="status_others" class="status_others history">status_others</th>
                                <th id="status_end" class="status_end history">status_end</th>
                                <th id="status_close" class="status_close history">status_close</th>
                                <th id="status_null" class="status_null history">status_null</th>
                            </tr>
                        </thead>
                        <tbody id="list-body">
END_OF_HEAD

for (my ($i, $ary_ref) = (1); $ary_ref = $sth->fetchrow_arrayref;
++$i) {
    my ($ticket_id, $line_id) = (@$ary_ref)[0, 1];
    my ($row_id) = "n$i-t$ticket_id-L$line_id";
    print(STDOUT "<tr id='$row_id' class='unselected-tr'>\n");
    for (my $index = 0; $index < @line_columns; ++$index) {
        my $column_name = $line_columns[$index];
        my $value = (@$ary_ref)[$index] || '';
        print(STDOUT "<td id='$row_id-$column_name' class='$column_name unhistorical'>$value</td>\n");
    }
    for (my $index = 0; $index < @status_columns; ++$index) {
        my $column_name = $status_columns[$index];
        my $value = (@$ary_ref)[$index + @line_columns];
        if ($value) {
            print(STDOUT "<td id='$row_id-$column_name-$value' class='$column_name has-history history'>$value</td>\n");
        } else {
            print(STDOUT "<td id='$row_id-$column_name-undef' class='$column_name no-history history'></td>\n");
        }
    }
    print(STDOUT "</tr>\n");
}

print STDOUT <<END_OF_FOOT;
                            <input name="ticket_id" type="hidden"/>
                            <input name="line_id" type="hidden"/>
                            <input name="history_id" type="hidden"/>
                            <input name="mode" type="hidden"/>
                            <input name="status" type="hidden"/>
                            <script src="../javascript/list.js"></script>
                        </tbody>
                    </table>
                </div>
                <button id="detail" name="detail" type="button" lang="en">detail</button>
            </form>
        </main>
    </body>
</html>
END_OF_FOOT
